# -*- coding=utf-8 -*-
import time
import jeepay
from jeepay.app_config import AppConfig

# 配置信息
api_key = "YLfBTJQB1rXVNgGk9VuGTwinpYnQWPoAK4EvgUd1DBcMKzzQ3UxpD6tcND7qlUVBjmwJ933U12u0bhPgr3QSxfK4U6zX9RomJlhT9kDooNDaW6hc2jpDnPWTZJGYCyHb"
AppConfig.set_mch_no("M1679219294")
AppConfig.set_app_id("6416da5ee4b00bed884be286")
AppConfig.set_api_key(api_key)

"""
支付 API 文档：https://docs.jeequan.com/docs/jeepay/payment_api
"""

print("支付")
try:
    create = jeepay.Pay.create(
        mchOrderNo="mho{}".format(int(time.time() * 1000)),  # 商户订单号
        wayCode="WX_BAR",  # 支付方式
        amount=1,  # 支付金额（单位分）
        currency="cny",  # 币种（目前只支持cny）
        clientIp="192.168.1.132",  # 发起支付请求客户端的IP地址
        subject="商品标题",  # 商品标题
        body="商品描述",  # 商品描述
        notifyUrl="",  # 异步通知地址
        returnUrl="",  # 前端跳转地址
        channelExtra="{\"auth_code\":\"134075943665965848\"}",  # 渠道扩展参数
        extParam=""  # 商户扩展参数,回调时原样返回
    )
    print(create)
except Exception as e:
    print(e)
